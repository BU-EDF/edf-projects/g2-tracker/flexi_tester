#!/usr/bin/python
import sqlite3 as sq
import sys
import argparse

parser = argparse.ArgumentParser(description='A program to read the database of test results.')
parser.add_argument('-D', '--DatabaseName', type=str, required=True, dest='databaseName', help='File name of the database for test results')
args = parser.parse_args()

con = sq.connect(args.databaseName)
while(1):
    mode = raw_input("Hit Enter for list of Flexi Tests. Enter Flexi_TestID to get list of that test's net tests. Enter 'q' to quit.")

    if(mode == ''):
        print '{:12s} {:20s} {:10s} {:5s} {:4s}'.format("Flexi_TestID","Date","User","FID","Verdict")
        with con:
            c = con.cursor()
            c.execute("SELECT * FROM Flexi_Tests")
            while True:
                row = c.fetchone()
                if row == None:
                    break
            
                print '{:12d} {:20s} {:10s} {:5s} {:4s}'.format(row[0],row[1],row[2],row[3],row[4])

    elif(mode == 'q'):
        break
    else:
        Flexi_TestID = mode
        netlist = []
        with con:
            c = con.cursor()
            c.execute("SELECT * FROM Net_Tests WHERE Flexi_TestID = ?",(Flexi_TestID,))
            while True:
                row = c.fetchone()
                if row == None:
                    break
                print(row [3]+ '\n' + row[4]+ "\n" + row[5] + "\n" + row[6] + "\n" + row[7] + '\n\n\n')
        
            


