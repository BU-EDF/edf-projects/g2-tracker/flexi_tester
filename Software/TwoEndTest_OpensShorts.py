#!/usr/bin/python
import serial
import time
import difflib
import datetime
import sqlite3 as sq
import argparse
import os.path
import sys

parser = argparse.ArgumentParser(description='A program to run tests and store results in database.')
parser.add_argument('-D', '--DatabaseName', type=str, required=True, dest='databaseName', help='File name of the database for test results.')
args = parser.parse_args()

if not os.path.isfile(args.databaseName):
   print args.databaseName, "does not exist. Check file name and create it with CreateDatabase.py if it doesn't exist."
   sys.exit(1)

testsnum = 46
ser = serial.Serial(port = "/dev/ttyACM0",baudrate = 115200)
con = sq.connect(args.databaseName)
c = con.cursor()

def insertChar(mystring, position, chartoinsert ):
    longi = len(mystring)
    mystring   =  mystring[:position] + chartoinsert + mystring[position:] 
    return mystring  

def WriteCommand(ser,line):
   if line.find("\n") != -1:
       raise cmd("new line in command")
   # write command and make sure the reponse makes sense                                                                                                                                                         

   ser.write(line)                                                                                                                                                                                                                                                               
   #execute the command and read the output                                                                                                                                                                                                                                          
   ser.write('\x0D')
   rd = ser.read()

   while rd != '\x0D':
      rd = ser.read()

   rd = ser.read()
   line = ""
   while rd != ">":
       line = line + rd
       rd = ser.read()
                           
   return line

def pull_net_test(testnum):
   c.execute('SELECT net_test, str_cmd_J1,str_cmd_J2 FROM test_types WHERE net_testID = ' + str(testnum))
   testinfo = c.fetchone()
    
   net_test = testinfo[0]
   str_cmd_J1 = testinfo[1]
   str_cmd_J2 = testinfo[2]
   
   return net_test, str_cmd_J1, str_cmd_J2

def generate_cmd(pin):
   #generate a test with pin set to zero
   line=list("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx")
   line[pin] = "0"
   return ''.join(line)

def generate_net(pins):
   #generate a test with pin set to zero
   line=list("111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111")
   for pin in pins:
      line[pin] = "0"
   return ''.join(line)



#reset the mega
ser.setDTR(False)
ser.setDTR(True)
time.sleep(0.1)
ser.setDTR(False)
#wait for the prompt from the mega before starting
rd = ser.read()
while rd != '>':
   rd = ser.read()

user = raw_input("Enter your name, enter q to quit:")
   
while(user != 'q'):
   #Test information inputted, quit with q
   Flexi_ID = raw_input("enter the Flexi cable ID number, enter q to quit:")
   if(Flexi_ID == 'q'):
      break
   time = datetime.datetime.now().strftime('%m/%d/%Y %H:%M:%S')
   
   #log username, Flexi_ID, and time of test

   c.execute("INSERT INTO Flexi_Tests VALUES(NULL,\"" + time +  "\",\"" + user + "\",\"" + Flexi_ID + "\",\"" + "Pending"+ "\")")
   c.execute('SELECT Flexi_TestID FROM Flexi_Tests WHERE Date = ?', (time,))
   Flexi_TestID = c.fetchone()[0]
   
   verdict = True
   #loop over the shield's 80 pin connector
   shorts = 0
   opens = 0
   for i in range(1,testsnum):
      #get test info from database
      net_test_info = pull_net_test(i)
      net = net_test_info[0]
      con1_pins = net_test_info[1]
      con2_pins = net_test_info[2]

      #generate the list of pins for this net
      snet_list1 = con1_pins.split(',')
      snet_list2 = con2_pins.split(',')   
      net_list=list()
      for snet in snet_list1:
         net_list.append(int(snet)-1)
      for snet in snet_list2:
         net_list.append(int(snet)-1+80)
   
      #generate the command for this test
      cmd = generate_cmd(net_list[0])
      #run test
      WriteCommand(ser,"c "+cmd)
      #get result
      test_result = WriteCommand(ser,'r')
      
      #update command string to look like a result string
      cmd= cmd.replace('x','1')
      expected = generate_net(net_list)
      result =  test_result.replace('\n','').replace('\r','')
         
      net_verdict = "PASS"
      net_verdict_bool = True
   
      #generate a string listing the differences 
      diff = ""
      for i in range(len(expected)):
         if expected[i] == result[i]:
            diff=diff+" "
         else:
            net_verdict = "FAIL"
            net_verdict_bool = False
            verdict = False
            if(expected[i] == '1' and result[i] == '0'):
               shorts = shorts+1
               diff=diff+"*"
            elif(expected[i] == '0' and result[i] == '1'):
               opens = opens+1
               diff=diff+"^"

      if(not net_verdict_bool):
         cmd_spaced= ''
         expected_spaced= ''
         result_spaced= ''
         diff_spaced = ''
         for i in range(0,9):
            cmd_spaced = cmd_spaced + cmd[i*16:(i+1)*16] + ' '
            expected_spaced = expected_spaced + expected[i*16:(i+1)*16] + ' '
            result_spaced = result_spaced + result[i*16:(i+1)*16] + ' ' 
            diff_spaced = diff_spaced + diff[(i*16):(i+1)*16] + ' ' 
            if(i == 4):
                cmd_spaced = cmd_spaced + "| "
                expected_spaced = expected_spaced + "| "
                result_spaced = result_spaced + "| "
                diff_spaced = diff_spaced + "| "
                
                
         
         
        
         print(net)
         print(cmd_spaced)
         print(expected_spaced)
         print(result_spaced)
         print(diff_spaced)

      c.execute("INSERT INTO Net_Tests VALUES(" + str(Flexi_TestID) + ",NULL,\"" + Flexi_ID + "\",\"" + net + "\",\"" + cmd + "\",\"" + expected + "\",\"" + result + "\",\"" + net_verdict + "\")")
            
   if(verdict):
      str_verdict = "PASS"
      print("PASS")

   else:
      str_verdict = "FAIL"
      print("FAIL")
      print("Disconnects:" + str(opens))
      print("Shorts:" + str(shorts))
      print("")
   
   c.execute("UPDATE Flexi_Tests SET Verdict =? WHERE Flexi_TestID =?", (str_verdict, Flexi_TestID))
   while(1):
       do_commit = raw_input("Save Results? (y or n)")
       if(do_commit == 'y'):
           con.commit()
           break
       elif(do_commit == 'n'):
           dump = c.execute("DELETE FROM Flexi_Tests WHERE Flexi_TestID=" + str(Flexi_TestID))
           dump = c.execute("DELETE FROM Net_Tests WHERE Flexi_TestID=" + str(Flexi_TestID))
           break
       else:
           print("Invalid input. Enter y or n.")
      

      

   
