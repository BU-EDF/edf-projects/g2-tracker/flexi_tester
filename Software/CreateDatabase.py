#!/usr/bin/python
import sqlite3 as sq
import os.path
import argparse

parser = argparse.ArgumentParser(description='A program to create the database to store test results.')
parser.add_argument('-D', '--DatabaseName', type=str, required=True, dest='databaseName', help='File name of the database for test results.')
args = parser.parse_args()

net_test = ["GND","p3V","n3V","p1.4V","TSTN2_IN","TREFE_IN","TREFO_IN","IBLR","DTHR","TEMP_P","TSTP2_IN","TSTN1_IN","TSTP1_IN","TDC_IN_10N","TDC_IN_9N","TDC_IN_8N","TDC_IN_15N","TDC_IN_15P","TDC_IN_14N","TDC_IN_14P","TDC_IN_13N","TDC_IN_13P","TDC_IN_12N","TDC_IN_11N","TDC_IN_9P","TDC_IN_6P","TDC_IN_3N","TDC_IN_12P","TDC_IN_11P","TDC_IN_5N","TDC_IN_1P","TDC_IN_0N","TDC_IN_0P","TDC_IN_8P","TDC_IN_5P","TDC_IN_2N","TDC_IN_4P","TDC_IN_4N","TDC_IN_2P","TDC_IN_10P","TDC_IN_6N","TDC_IN_7P","TDC_IN_7N","TDC_IN_1N","TDC_IN_3P"]

str_cmd_J1 = ["50,37,38,28,76,77,78,48,79,39,1,44,46,42,12,41,80,52,40,4,3,2,36,26,16,74,34,24,14,43,62","20,18,22","6,8,10","30,32","70","60","58","56","54","72","68","66","64","55","51","47","75","73","71","69","67","65","63","59","49","29","19","61","57","27","9","7","5","45","25","15","21","23","13","53","31","33","35","11","17"]

str_cmd_J2 = ["12,32,52,62,13,33,55,57,35,54,34,53,31,11,30,6,58,61","59,60","63,64","56","7","3","4","1","2","5","8","9","10","24","26","28","14","15","16","17","18","19","20","22","27","39","44","21","23","40","49","50","51","29","41","46","43","42","47","25","38","37","36","48","45"]


if(os.path.isfile(args.databaseName)):
   print args.databaseName, "already exists, delete before trying again."
    
else:
   conn=sq.connect(args.databaseName)
   c = conn.cursor()
   c.execute("CREATE TABLE Test_Types(net_testID INTEGER PRIMARY KEY, net_test TEXT, str_cmd_J1 TEXT, str_cmd_J2 TEXT);")
   c.execute("CREATE TABLE Flexi_Tests(Flexi_TestID INTEGER PRIMARY KEY, Date DATE, Username TEXT, FID TEXT, Verdict TEXT)")
   c.execute("CREATE TABLE Net_Tests(Flexi_TestID INTEGER, Net_TestID INTEGER PRIMARY KEY, FID TEXT, Net TEXT, Command TEXT, Net_Pins TEXT, Return TEXT, Verdict TEXT)")

   for i in range(len(net_test)):
         conn.execute("INSERT INTO Test_Types VALUES(NULL,\"" +net_test[i]+"\",\""+str_cmd_J1[i]+"\",\""+str_cmd_J2[i]+"\")")
   conn.commit()
   conn.close()
   print args.databaseName, "created."

   



