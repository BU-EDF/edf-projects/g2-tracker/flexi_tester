#include <Wire.h>
#include <Centipede.h>
#include <stdint.h>
#define KEYWORD 2
#define ARDUINO_OFFSET 22

Centipede CS;
int const size = 146; // 144 pins + 'c '
char buffer[size+1]; //creates char array, with null character accomodated for
int iBuffer = 0; // buffer cursor


// checks if characters inputted into buffer are valid
bool checkval(char input){ 
  switch(input){
  case '1':
  case '0':
  case 'x':
    return true;
    break;
      
  default:
    return false;
    break;
  }
}

void setLED(char color){
  switch(color){
    case 'r':
      digitalWrite(8, HIGH);
      break;
    case 'o':
      digitalWrite(9, HIGH);
      break;
    case 'g':
      digitalWrite(10, HIGH);

      break;
  }
}

// sets the pins according to what's inputted into the buffer. x = high impedence, 0 = low, 1 = high 
void setpins(char settings[147]){
  //int arduino_offset = 24; //only the last 16 pins on the arduino are used, this is the offset of the pins
  //int conn0size = 80; //number of pins on first end
  //int portsize = 15; //number of pins per port on each centipede

  if(iBuffer<size){
    Serial.write("Too few characters, try again.");
    return;
  }
    
  for(int i = 2; i<size;i++){
    if(!(checkval(settings[i]))){
      Serial.write("ERROR: Invalid characters, try again");
      return;
    } 
  }

  int port = -1; //port cursor
  uint16_t centimode [] = {0,0,0,0,0,0,0,0}; //each element corresponds to a port
  uint16_t centipullup [] = {0,0,0,0,0,0,0,0};
  uint16_t centioutput [] = {0,0,0,0,0,0,0,0};

  //Get pointers to connector strings
  char * pinString = settings + KEYWORD;


  //list of positions in the CLI string argument for each centipede pin - organised such that array position gives pin
  // e.g. Centipede pin 0 (port 0, pin 0) corresponds to element 33 in pinString

  // Dan's pin ordering
  //int centipedeToConnector [] {33,35,37,39,41,43,45,47,63,61,59,57,55,53,51,49,65,67,69,71,73,75,77,79,64,66,68,70,72,74,76,78,62,60,58,56,54,52,50,48,32,34,36,38,40,42,44,46,30,28,26,24,22,20,18,16,0,2,4,6,8,10,12,14,
  //                               94,92,90,88,86,84,82,80,81,83,85,87,89,91,93,95,110,108,106,104,102,100,98,96,97,99,101,103,105,107,109,111,126,124,122,120,118,116,114,112,113,115,117,119,121,123,125,127,142,140,138,136,134,132,130,128,129,131,133,135,137,139,141,143};
                               
  // James' updated ordering - should be correct for writing to ZIF connector pins too now (only changed above pin 80)
  int centipedeToConnector [] {33,35,37,39,41,43,45,47,63,61,59,57,55,53,51,49,65,67,69,71,73,75,77,79,64,66,68,70,72,74,76,78,62,60,58,56,54,52,50,48,32,34,36,38,40,42,44,46,30,28,26,24,22,20,18,16,0,2,4,6,8,10,12,14,
                                 80,82,84,86,88,90,92,94,95,93,91,89,87,85,83,81,96,98,100,102,104,106,108,110,111,109,107,105,103,101,99,97,112,114,116,118,120,122,124,126,127,125,123,121,119,117,115,113,128,130,132,134,136,138,140,142,143,141,139,137,135,133,131,129};
    
  int arduinoToConnector [] {1,3,5,7,9,11,13,15,17,19,21,23,25,27,29,31};

  //centipede 1 and 2
  for(int port = 0; port < 8 ;port++){
    for(int port_pin = 0; port_pin  < 16;port_pin++){
      char pinSetting = pinString[centipedeToConnector[(port << 4) | (port_pin&0xF)]];  //compute the position in centipedeToConnector and then use that to find where in he pinString to look for this settinggggg
      switch (pinSetting){
        case '1':
        case '0':
          centimode[port]   &= ~(1 << (0xF & (port_pin))); //set mode to output
          centipullup[port] &= ~(1 << (0xF & (port_pin))); //disable the pull up
          //set the output level
          if(pinSetting == '1'){
            centioutput[port] |=   1 << ((0xF & port_pin));  
          }else{
            centioutput[port] &= ~(1 << ((0xF & port_pin)));  
          }
          break;
        case 'x':
        case 'X':
        default:
          centimode[port]   |= 1 << (0xF & (port_pin)); //set mode to input
          centipullup[port] |= 1 << (0xF & (port_pin)); //enable pull-up
          break;
      }     
    }
   CS.portMode(port,centimode[port]);
   CS.portPullup(port,centipullup[port]);
   CS.portWrite(port,centioutput[port]);   
  }

  for (int i = 0; i<16;i++){
    char pinSetting = pinString[arduinoToConnector[i]];
     switch (pinSetting){
        case '1':
        case '0':
          pinMode((i+ARDUINO_OFFSET), 1);//Output (no pullup)          
          
          //set the output level
          if(pinSetting == '1'){
            digitalWrite(i+ARDUINO_OFFSET,1);
          }else{
            digitalWrite(i+ARDUINO_OFFSET,0);            
          }
          break;
        case 'x':
        case 'X':
        default:
          //set pin to input with a pullup
          pinMode(i+ARDUINO_OFFSET,INPUT_PULLUP);
          break;
      }     
  }
}

void readpins(){
  int conn0size = 80;
  //int pinAorC [] = {0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}; // 0 = arduino, 1 = centipede
  int pinAorC [] = {1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}; // 0 = arduino, 1 = centipede
  int pinout80 [] = {56,22,57,23,58,24,59,25,60,26,61,27,62,28,63,29,55,30,54,31,53,32,52,33,51,34,50,35,49,36,48,37,40,0,41,1,42,2,43,3,44,4,45,5,46,6,47,7,39,15,38,14,37,13,36,12,35,11,34,10,33,9,32,8,24,16,25,17,26,18,27,19,28,20,29,21,30,22,31,23};  
  //int pinout64 [] = {56,55,57,54,58,53,59,52,60,51,61,50,62,49,63,48,40,39,41,38,42,37,43,36,44,35,45,34,46,33,47,32,24,23,25,22,26,21,27,20,28,19,29,18,30,17,31,16,8,7,9,6,10,5,11,4,12,3,13,2,14,1,15,0};
  int pinout64 [] = {0,15,1,14,2,13,3,12,4,11,5,10,6,9,7,8,16,31,17,30,18,29,19,28,20,27,21,26,22,25,23,24,32,47,33,46,34,45,35,44,36,43,37,42,38,41,39,40,48,63,49,62,50,61,51,60,52,59,53,58,54,57,55,56};

  for (int connpin = 0; connpin<144;connpin++){
    if(connpin<conn0size){
      if(pinAorC[connpin]==0){
           Serial.print(digitalRead(pinout80[connpin]));
      } else {
           Serial.print(CS.digitalRead(pinout80[connpin]));
      }
    } else {
        Serial.print(CS.digitalRead(pinout64[connpin-80]+64));
    }
  }
  
//  Serial.write(0xA);
//  Serial.write(0xD);
//  Serial.write('>');
  
}

void setup() {
  Serial.begin(115200);
  
  Wire.begin();
  CS.initialize();

  for(int i = 8;i<11;i++){
    pinMode(i, OUTPUT);
  }

  for (int i = 0;i<8;i++){
  CS.portMode (i,0b1111111111111111);
  CS.portWrite(i,0b1111111111111111);
  }
  Serial.print('>');
} 

void loop() {
    // put your main code here, to run repeatedly:
  /*
  Serial.write("at top of loop");
  Serial.write(0xA);
  Serial.write(0xD);*/
  if(Serial.available() > 0){
    /*
    Serial.write("Serial is available");
    Serial.write(0xA);
    Serial.write(0xD);
    */
    char input;

    input = Serial.read();
    Serial.write(input);

    
    //if((input <= 'z' && input >= 'a')||(input <= 'Z' && input >= 'A')||(input>= '0' && input <= '9')|| input == ' '){
    if((input <= '~') && (input >= ' ')){
     /*
      Serial.write("input detected");
      Serial.write(0xA);
      Serial.write(0xD);
      */
      buffer[iBuffer] = input;
      iBuffer++;
      
    } else if(input == '\b'){      
      if(iBuffer != 0){
        Serial.write(' ');
        Serial.write('\b');
        //Serial.write(8);
        iBuffer--;
        buffer[iBuffer] = ' ';        
        }
            
      }else if(input == 0xD){
        Serial.write(0xA);
        //Serial.write(0xD);

  
        switch(buffer[0]){
          case 'c':
//            Serial.print("H");
            setpins(buffer);
            //Serial.write(0xA);
            //Serial.write(0xD);
            break;
                  
          case 'r':
            readpins();
            //Serial.write(0xA);
            //Serial.write(0xD);
            break;
            
          default:
            if(iBuffer >1){
              Serial.write("Bad command, try again");
              break; 
            } else{
              break;
            }
        }  
      iBuffer = 0;
      memset(buffer,0,size);
      Serial.write(0xA);
      Serial.write(0xD);
      Serial.write('>');
    }
  
    if(iBuffer >(size)){
      iBuffer=0;
      Serial.write("ERROR: Buffer overflow, try again.");
      Serial.write(0xA);
      Serial.write(0xD);
      Serial.write('>');
      memset(buffer,0,size);
    }
   
  }
  
}


