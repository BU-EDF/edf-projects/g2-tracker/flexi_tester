import serial
import time
import difflib
import datetime

net_test = ["GND","p3V","n3V","p1.4V","TSTN2_IN","TREFE_IN","TREFO_IN","IBLR","DTHR","TEMP_P","TSTP2_IN","TSTN1_IN","TSTP1_IN","TDC_IN_10N","TDC_IN_9N","TDC_IN_8N","TDC_IN_15N","TDC_IN_15P","TDC_IN_14N","TDC_IN_14P","TDC_IN_13N","TDC_IN_13P","TDC_IN_12N","TDC_IN_11N","TDC_IN_9P","TDC_IN_6P","TDC_IN_3N","TDC_IN_12P","TDC_IN_11P","TDC_IN_5N","TDC_IN_1P","TDC_IN_0N","TDC_IN_0P","TDC_IN_8P","TDC_IN_5P","TDC_IN_2N","TDC_IN_4P","TDC_IN_4N","TDC_IN_2P","TDC_IN_10P","TDC_IN_6N","TDC_IN_7P","TDC_IN_7N","TDC_IN_1N","TDC_IN_3P"]

con1_pins = ["50,37,38,28,76,77,78,48,79,39,1,44,46,42,12,41,80,52,40,4,3,2,36,26,16,74,34,24,14,43,62","20,18,22","6,8,10","30,32","70","60","58","56","54","72","68","66","64","55","51","47","75","73","71","69","67","65","63","59","49","29","19","61","57","27","9","7","5","45","25","15","21","23","13","53","31","33","35","11","17"]

con2_pins = ["12,32,52,62,13,33,55,57,35,54,34,53,31,11,30,6,58,61","59,60","63,64","56","7","3","4","1","2","5","8","9","10","24","26","28","14","15","16","17","18","19","20","22","27","39","44","21","23","40","49","50","51","29","41","46","43","42","47","25","38","37","36","48","45"]

ser = serial.Serial(port = "/dev/ttyACM0",baudrate = 115200)

def WriteCommand(ser,line):
   if line.find("\n") != -1:
       raise cmd("new line in command")
   # write command and make sure the reponse makes sense                                                                                                                                                         

   ser.write(line)                                                                                                                                                                                                                                                               
   #execute the command and read the output                                                                                                                                                                                                                                          
   ser.write('\x0D')
   rd = ser.read()

   while rd != '\x0D':
      rd = ser.read()

   rd = ser.read()
   line = ""
   while rd != ">":
       line = line + rd
       rd = ser.read()
                           
   return line



def generate_cmd(pin):
   #generate a test with pin set to zero
   line=list("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx")
   line[pin] = "0"
   return ''.join(line)

def generate_net(pins):
   #generate a test with pin set to zero
   line=list("111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111")
   for pin in pins:
      line[pin] = "0"
   return ''.join(line)



#reset the mega
ser.setDTR(False)
ser.setDTR(True)
time.sleep(0.1)
ser.setDTR(False)
#wait for the prompt from the mega before starting
rd = ser.read()
while rd != '>':
   rd = ser.read()

user = raw_input("Enter your name, enter q to quit:")
   
while(user != 'q'):
   #Test information inputted, quit with q
   Flexi_ID = raw_input("enter the Flexi cable ID number, enter q to quit:")
   if(Flexi_ID == 'q'):
      break
   time = datetime.datetime.now().strftime('%m/%d/%Y %H:%M')
   
   #log username, Flexi_ID, and time of test
   with open("Test_Results.txt", "a") as testinfo:
      testinfo.write(time + "\n" + user + "\n" + Flexi_ID + "\n")
   
   verdict = True
   #loop over the shield's 80 pin connector
   for net in range(len(net_test)):
   
      #generate the list of pins for this net
      snet_list1 = con1_pins[net].split(',')
      snet_list2 = con2_pins[net].split(',')   
      net_list=list()
      for snet in snet_list1:
         net_list.append(int(snet)-1)
      for snet in snet_list2:
         net_list.append(int(snet)-1+80)
   
      #generate the command for this test
      cmd = generate_cmd(net_list[0])
      #run test
      WriteCommand(ser,"c "+cmd)
      #get result
      test_result = WriteCommand(ser,'r')
      
      #update command string to look like a result string
      cmd= cmd.replace('x','1')
      expected = generate_net(net_list)
      result =  test_result.replace('\n','').replace('\r','')
         
      net_verdict = True
   
      #generate a string listing the differences 
      diff = ""
      for i in range(len(expected)):
         if expected[i] == result[i]:
            diff=diff+" "
         else:
            diff=diff+"*"
            net_verdict = False
            verdict = False
      
      
      if(not net_verdict):
         with open("Test_Results.txt", "a") as testinfo:
            testinfo.write(net_test[net] + "\n" + cmd + "\n" + expected + "\n" + result + "\n" + diff + "\n \n")
            
   
   if(verdict):
      print("PASS")
      with open("Test_Results.txt", "a") as testinfo:
         testinfo.write("PASS" + "" + "")
   else:
      print("FAIL")
      with open("Test_Results.txt", "a") as testinfo:
         testinfo.write("FAIL"+ "" + "")

   
