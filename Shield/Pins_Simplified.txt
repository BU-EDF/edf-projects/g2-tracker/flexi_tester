

Arduino   Connector Pin

23        1
24        3
25        5
26        7
27        9
28        11
29        13
30        15
31        17
32        19
33        21
34        23
35        25
36        27
37        29
38        31


Shield    Connector Pin

1         33
2         35
3         37
4         39
5         41
6         43
7         45
8         47
9         63
10        61
11        59
12        57
13        55
14        53
15        51
16        49
17        65
18        67
19        69
20        71
21        73
22        75
23        77
24        79
25        66
26        68
27        70
28        72
29        74
30        76
31        78
32        80
33        64
34        62
35        60
36        58
37        56
38        54
39        52
40        50
41        34
42        36
43        38
44        40
45        42
46        44
47        46
48        48
49        32
50        30
51        28
52        26
53        24
54        22
55        20
56        18
57        2
58        4
59        6
60        8
61        10
62        12
63        14
64        16


Arduino   LED (Anode)
N$89      LED 1
N$90      LED 2
N$91      LED 3