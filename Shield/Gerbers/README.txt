4 Layer FR-4
Final thckness 0.063

Fexi_Tester_Drill		NC drill
Flexi_Tester.cmp		Layer 1 (top)
Flexi_Tester.crc		Top solder paste
Flexi_Tester.crs		-ignore-
Flexi_Tester.l15		Layer 2
Flexi_Tester.ly2		Layer 3
Flexi_Tester.pls		-ignore-
Flexi_Tester_Silk		Top silkscreen
Flexi_Tester_Silk.gpi		-ignore-
Flexi_Tester.sol		Layer 4
Flexi_Tester.stc		Top solder mask
Flexi_Tester.sts		Bottom solder mask

