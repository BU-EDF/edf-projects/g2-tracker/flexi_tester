EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:flexi_tester_external
LIBS:flexi_tester_external-cache
EELAYER 25 0
EELAYER END
$Descr A 11000 8500
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CONN_02X05 P3
U 1 1 571E60EA
P 8750 3350
F 0 "P3" H 8750 3650 50  0000 C CNN
F 1 "CONN_02X05" H 8750 3050 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x05" H 8750 2150 50  0001 C CNN
F 3 "" H 8750 2150 50  0000 C CNN
	1    8750 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 5900 5450 5950
Wire Wire Line
	5450 5950 5650 5950
Wire Wire Line
	5650 5950 5650 5900
Wire Wire Line
	8500 3150 8050 3150
Wire Wire Line
	9000 3150 9450 3150
Wire Wire Line
	9450 3150 9450 3650
Wire Wire Line
	9000 3350 9050 3350
Wire Wire Line
	9050 3350 9050 3250
Wire Wire Line
	9000 3250 9450 3250
Connection ~ 9450 3250
Connection ~ 9050 3250
Wire Wire Line
	9200 3450 9000 3450
Wire Wire Line
	9000 3550 9450 3550
Connection ~ 9450 3550
$Comp
L +5V #PWR01
U 1 1 571E838F
P 9200 3450
F 0 "#PWR01" H 9200 3300 50  0001 C CNN
F 1 "+5V" H 9200 3590 50  0000 C CNN
F 2 "" H 9200 3450 50  0000 C CNN
F 3 "" H 9200 3450 50  0000 C CNN
	1    9200 3450
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR02
U 1 1 571E83BD
P 8200 3450
F 0 "#PWR02" H 8200 3300 50  0001 C CNN
F 1 "+5V" H 8200 3590 50  0000 C CNN
F 2 "" H 8200 3450 50  0000 C CNN
F 3 "" H 8200 3450 50  0000 C CNN
	1    8200 3450
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR03
U 1 1 571E8421
P 4750 5350
F 0 "#PWR03" H 4750 5200 50  0001 C CNN
F 1 "+5V" H 4750 5490 50  0000 C CNN
F 2 "" H 4750 5350 50  0000 C CNN
F 3 "" H 4750 5350 50  0000 C CNN
	1    4750 5350
	1    0    0    -1  
$EndComp
Text Label 8300 3250 0    60   ~ 0
SCL
Text Label 8300 3350 0    60   ~ 0
SDA
Wire Wire Line
	8300 3250 8500 3250
Wire Wire Line
	8500 3350 8300 3350
Wire Wire Line
	8200 3450 8500 3450
Wire Wire Line
	8050 3150 8050 3650
Wire Wire Line
	8500 3550 8050 3550
Connection ~ 8050 3550
Text Label 6300 5350 2    60   ~ 0
SDA
Text Label 6300 5450 2    60   ~ 0
SCL
Wire Wire Line
	6050 5350 6300 5350
Wire Wire Line
	6050 5450 6300 5450
Connection ~ -6000 4000
$Comp
L FH28D-64S-0.5SH(98) P1
U 1 1 571E927F
P 1100 4150
F 0 "P1" H 1150 7400 60  0000 C CNN
F 1 "FH28D-64S-0.5SH(98)" V 1150 4150 60  0000 C CNN
F 2 "flexi_tester_external:FH28D-64S-0.5SH(98)" H -1500 6200 60  0001 C CNN
F 3 "" H -1500 6200 60  0000 C CNN
	1    1100 4150
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR04
U 1 1 571E6026
P 4750 5450
F 0 "#PWR04" H 4750 5200 50  0001 C CNN
F 1 "GND" H 4750 5300 50  0000 C CNN
F 2 "" H 4750 5450 50  0000 C CNN
F 3 "" H 4750 5450 50  0000 C CNN
	1    4750 5450
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR05
U 1 1 571E6079
P 9450 3650
F 0 "#PWR05" H 9450 3400 50  0001 C CNN
F 1 "GND" H 9450 3500 50  0000 C CNN
F 2 "" H 9450 3650 50  0000 C CNN
F 3 "" H 9450 3650 50  0000 C CNN
	1    9450 3650
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR06
U 1 1 571E60BD
P 8050 3650
F 0 "#PWR06" H 8050 3400 50  0001 C CNN
F 1 "GND" H 8050 3500 50  0000 C CNN
F 2 "" H 8050 3650 50  0000 C CNN
F 3 "" H 8050 3650 50  0000 C CNN
	1    8050 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 5450 4850 5450
Wire Wire Line
	4750 5350 4850 5350
Text Label 1800 1000 2    60   ~ 0
1
Text Label 1800 1100 2    60   ~ 0
2
Text Label 1800 1200 2    60   ~ 0
3
Text Label 1800 1300 2    60   ~ 0
4
Text Label 1800 1400 2    60   ~ 0
5
Text Label 1800 1500 2    60   ~ 0
6
Text Label 1800 1600 2    60   ~ 0
7
Text Label 1800 1700 2    60   ~ 0
8
Text Label 1800 1800 2    60   ~ 0
9
Text Label 1800 1900 2    60   ~ 0
10
Text Label 1800 2000 2    60   ~ 0
11
Text Label 1800 2100 2    60   ~ 0
12
Text Label 1800 2200 2    60   ~ 0
13
Text Label 1800 2300 2    60   ~ 0
14
Text Label 1800 2400 2    60   ~ 0
15
Text Label 1800 2500 2    60   ~ 0
16
Text Label 1800 2600 2    60   ~ 0
17
Text Label 1800 2700 2    60   ~ 0
18
Text Label 1800 2800 2    60   ~ 0
19
Text Label 1800 2900 2    60   ~ 0
20
Text Label 1800 3000 2    60   ~ 0
21
Text Label 1800 3100 2    60   ~ 0
22
Text Label 1800 3200 2    60   ~ 0
23
Text Label 1800 3300 2    60   ~ 0
24
Text Label 1800 3400 2    60   ~ 0
25
Text Label 1800 3500 2    60   ~ 0
26
Text Label 1800 3600 2    60   ~ 0
27
Text Label 1800 3700 2    60   ~ 0
28
Text Label 1800 3800 2    60   ~ 0
29
Text Label 1800 3900 2    60   ~ 0
30
Text Label 1800 4000 2    60   ~ 0
31
Text Label 1800 4100 2    60   ~ 0
32
Text Label 1800 4200 2    60   ~ 0
33
Text Label 1800 4300 2    60   ~ 0
34
Text Label 1800 4400 2    60   ~ 0
35
Text Label 1800 4500 2    60   ~ 0
36
Text Label 1800 4600 2    60   ~ 0
37
Text Label 1800 4700 2    60   ~ 0
38
Text Label 1800 4800 2    60   ~ 0
39
Text Label 1800 4900 2    60   ~ 0
40
Text Label 1800 5000 2    60   ~ 0
41
Text Label 1800 5100 2    60   ~ 0
42
Text Label 1800 5200 2    60   ~ 0
43
Text Label 1800 5300 2    60   ~ 0
44
Text Label 1800 5400 2    60   ~ 0
45
Text Label 1800 5500 2    60   ~ 0
46
Text Label 1800 5600 2    60   ~ 0
47
Text Label 1800 5700 2    60   ~ 0
48
Text Label 1800 5800 2    60   ~ 0
49
Text Label 1800 5900 2    60   ~ 0
50
Text Label 1800 6000 2    60   ~ 0
51
Text Label 1800 6100 2    60   ~ 0
52
Text Label 1800 6200 2    60   ~ 0
53
Text Label 1800 6300 2    60   ~ 0
54
Text Label 1800 6400 2    60   ~ 0
55
Text Label 1800 6500 2    60   ~ 0
56
Text Label 1800 6600 2    60   ~ 0
57
Text Label 1800 6700 2    60   ~ 0
58
Text Label 1800 6800 2    60   ~ 0
59
Text Label 1800 6900 2    60   ~ 0
60
Text Label 1800 7000 2    60   ~ 0
61
Text Label 1800 7100 2    60   ~ 0
62
Text Label 1800 7200 2    60   ~ 0
63
Text Label 1800 7300 2    60   ~ 0
64
Wire Wire Line
	1600 1000 1800 1000
Wire Wire Line
	1600 7300 1800 7300
Wire Wire Line
	1800 7200 1600 7200
Wire Wire Line
	1800 7100 1600 7100
Wire Wire Line
	1600 7000 1800 7000
Wire Wire Line
	1800 6900 1600 6900
Wire Wire Line
	1600 6800 1800 6800
Wire Wire Line
	1800 6700 1600 6700
Wire Wire Line
	1600 6600 1800 6600
Wire Wire Line
	1800 6500 1600 6500
Wire Wire Line
	1600 6400 1800 6400
Wire Wire Line
	1800 6300 1600 6300
Wire Wire Line
	1600 6200 1800 6200
Wire Wire Line
	1800 6100 1600 6100
Wire Wire Line
	1600 6000 1800 6000
Wire Wire Line
	1800 5900 1600 5900
Wire Wire Line
	1600 5800 1800 5800
Wire Wire Line
	1800 5700 1600 5700
Wire Wire Line
	1600 5600 1800 5600
Wire Wire Line
	1800 5500 1600 5500
Wire Wire Line
	1600 5400 1800 5400
Wire Wire Line
	1800 5300 1600 5300
Wire Wire Line
	1600 5200 1800 5200
Wire Wire Line
	1800 5100 1600 5100
Wire Wire Line
	1600 5000 1800 5000
Wire Wire Line
	1800 4900 1600 4900
Wire Wire Line
	1600 4800 1800 4800
Wire Wire Line
	1800 4700 1600 4700
Wire Wire Line
	1600 4600 1800 4600
Wire Wire Line
	1800 4500 1600 4500
Wire Wire Line
	1600 4400 1800 4400
Wire Wire Line
	1800 4300 1600 4300
Wire Wire Line
	1800 4200 1600 4200
Wire Wire Line
	1600 4100 1800 4100
Wire Wire Line
	1800 4000 1600 4000
Wire Wire Line
	1600 3900 1800 3900
Wire Wire Line
	1600 3800 1800 3800
Wire Wire Line
	1800 3700 1600 3700
Wire Wire Line
	1600 3600 1800 3600
Wire Wire Line
	1800 3500 1600 3500
Wire Wire Line
	1600 3400 1800 3400
Wire Wire Line
	1800 3300 1600 3300
Wire Wire Line
	1600 3200 1800 3200
Wire Wire Line
	1800 3100 1600 3100
Wire Wire Line
	1600 3000 1800 3000
Wire Wire Line
	1800 2900 1600 2900
Wire Wire Line
	1600 2800 1800 2800
Wire Wire Line
	1800 2700 1600 2700
Wire Wire Line
	1600 2600 1800 2600
Wire Wire Line
	1800 2500 1600 2500
Wire Wire Line
	1600 2400 1800 2400
Wire Wire Line
	1800 2300 1600 2300
Wire Wire Line
	1600 2200 1800 2200
Wire Wire Line
	1800 2100 1600 2100
Wire Wire Line
	1600 2000 1800 2000
Wire Wire Line
	1800 1900 1600 1900
Wire Wire Line
	1600 1800 1800 1800
Wire Wire Line
	1800 1700 1600 1700
Wire Wire Line
	1600 1600 1800 1600
Wire Wire Line
	1800 1500 1600 1500
Wire Wire Line
	1600 1400 1800 1400
Wire Wire Line
	1800 1300 1600 1300
Wire Wire Line
	1600 1200 1800 1200
Wire Wire Line
	1800 1100 1600 1100
Text Label 4700 2750 0    60   ~ 0
49
Text Label 4700 2650 0    60   ~ 0
50
Text Label 4700 2850 0    60   ~ 0
51
Text Label 4700 2550 0    60   ~ 0
52
Text Label 4700 2950 0    60   ~ 0
53
Text Label 4700 2450 0    60   ~ 0
54
Text Label 4700 3050 0    60   ~ 0
55
Text Label 4700 2350 0    60   ~ 0
56
Text Label 4700 3150 0    60   ~ 0
57
Text Label 4700 2250 0    60   ~ 0
58
Text Label 4700 3250 0    60   ~ 0
59
Text Label 4700 2150 0    60   ~ 0
60
Text Label 4700 3350 0    60   ~ 0
61
Text Label 4700 2050 0    60   ~ 0
62
Text Label 4700 3450 0    60   ~ 0
63
Text Label 4700 1950 0    60   ~ 0
64
Wire Wire Line
	4700 3450 4850 3450
Wire Wire Line
	4850 3350 4700 3350
Wire Wire Line
	4700 3250 4850 3250
Wire Wire Line
	4850 3150 4700 3150
Wire Wire Line
	4700 3050 4850 3050
Wire Wire Line
	4850 2950 4700 2950
Wire Wire Line
	4700 2850 4850 2850
Wire Wire Line
	4850 2750 4700 2750
Wire Wire Line
	4700 2650 4850 2650
Wire Wire Line
	4850 2550 4700 2550
Wire Wire Line
	4700 2450 4850 2450
Wire Wire Line
	4700 2250 4850 2250
Wire Wire Line
	4850 2350 4700 2350
Wire Wire Line
	4700 2150 4850 2150
Wire Wire Line
	4700 2050 4850 2050
Wire Wire Line
	4700 1950 4850 1950
Text Label 4700 4450 0    60   ~ 0
33
Text Label 4700 4350 0    60   ~ 0
34
Text Label 4700 4550 0    60   ~ 0
35
Text Label 4700 4250 0    60   ~ 0
36
Text Label 4700 4650 0    60   ~ 0
37
Text Label 4700 4150 0    60   ~ 0
38
Text Label 4700 4750 0    60   ~ 0
39
Text Label 4700 4050 0    60   ~ 0
40
Text Label 4700 4850 0    60   ~ 0
41
Text Label 4700 3950 0    60   ~ 0
42
Text Label 4700 4950 0    60   ~ 0
43
Text Label 4700 3850 0    60   ~ 0
44
Text Label 4700 5050 0    60   ~ 0
45
Text Label 4700 3750 0    60   ~ 0
46
Text Label 4700 5150 0    60   ~ 0
47
Text Label 4700 3650 0    60   ~ 0
48
Wire Wire Line
	4700 3650 4850 3650
Wire Wire Line
	4850 3750 4700 3750
Wire Wire Line
	4700 3850 4850 3850
Wire Wire Line
	4850 3950 4700 3950
Wire Wire Line
	4700 4050 4850 4050
Wire Wire Line
	4700 4150 4850 4150
Wire Wire Line
	4850 4250 4700 4250
Wire Wire Line
	4700 4350 4850 4350
Wire Wire Line
	4850 4450 4700 4450
Wire Wire Line
	4700 4550 4850 4550
Wire Wire Line
	4700 4650 4850 4650
Wire Wire Line
	4850 4750 4700 4750
Wire Wire Line
	4700 4850 4850 4850
Wire Wire Line
	4850 4950 4700 4950
Wire Wire Line
	4700 5050 4850 5050
Wire Wire Line
	4850 5150 4700 5150
Text Label 6250 4350 2    60   ~ 0
17
Text Label 6250 4450 2    60   ~ 0
18
Text Label 6250 4250 2    60   ~ 0
19
Text Label 6250 4550 2    60   ~ 0
20
Text Label 6250 4150 2    60   ~ 0
21
Text Label 6250 4650 2    60   ~ 0
22
Text Label 6250 4050 2    60   ~ 0
23
Text Label 6250 4750 2    60   ~ 0
24
Text Label 6250 3950 2    60   ~ 0
25
Text Label 6250 4850 2    60   ~ 0
26
Text Label 6250 3850 2    60   ~ 0
27
Text Label 6250 4950 2    60   ~ 0
28
Text Label 6250 3750 2    60   ~ 0
29
Text Label 6250 5050 2    60   ~ 0
30
Text Label 6250 3650 2    60   ~ 0
31
Text Label 6250 5150 2    60   ~ 0
32
Wire Wire Line
	6250 5150 6050 5150
Wire Wire Line
	6050 5050 6250 5050
Wire Wire Line
	6050 4950 6250 4950
Wire Wire Line
	6250 4850 6050 4850
Wire Wire Line
	6050 4750 6250 4750
Wire Wire Line
	6250 4650 6050 4650
Wire Wire Line
	6050 4550 6250 4550
Wire Wire Line
	6250 4450 6050 4450
Wire Wire Line
	6050 4350 6250 4350
Wire Wire Line
	6250 4250 6050 4250
Wire Wire Line
	6050 4150 6250 4150
Wire Wire Line
	6250 4050 6050 4050
Wire Wire Line
	6050 3950 6250 3950
Wire Wire Line
	6250 3850 6050 3850
Wire Wire Line
	6050 3750 6250 3750
Wire Wire Line
	6250 3650 6050 3650
Text Label 6250 2650 2    60   ~ 0
1
Text Label 6250 2750 2    60   ~ 0
2
Text Label 6250 2550 2    60   ~ 0
3
Text Label 6250 2850 2    60   ~ 0
4
Text Label 6250 2450 2    60   ~ 0
5
Text Label 6250 2950 2    60   ~ 0
6
Text Label 6250 2350 2    60   ~ 0
7
Text Label 6250 3050 2    60   ~ 0
8
Text Label 6250 2250 2    60   ~ 0
9
Text Label 6250 3150 2    60   ~ 0
10
Text Label 6250 2150 2    60   ~ 0
11
Text Label 6250 3250 2    60   ~ 0
12
Text Label 6250 2050 2    60   ~ 0
13
Text Label 6250 3350 2    60   ~ 0
14
Text Label 6250 1950 2    60   ~ 0
15
Text Label 6250 3450 2    60   ~ 0
16
Wire Wire Line
	6050 1950 6250 1950
Wire Wire Line
	6250 3450 6050 3450
Wire Wire Line
	6050 3350 6250 3350
Wire Wire Line
	6250 3250 6050 3250
Wire Wire Line
	6050 3150 6250 3150
Wire Wire Line
	6250 3050 6050 3050
Wire Wire Line
	6050 2950 6250 2950
Wire Wire Line
	6250 2850 6050 2850
Wire Wire Line
	6050 2750 6250 2750
Wire Wire Line
	6250 2650 6050 2650
Wire Wire Line
	6050 2550 6250 2550
Wire Wire Line
	6250 2450 6050 2450
Wire Wire Line
	6050 2350 6250 2350
Wire Wire Line
	6250 2250 6050 2250
Wire Wire Line
	6050 2150 6250 2150
Wire Wire Line
	6250 2050 6050 2050
NoConn ~ 5250 5900
$Comp
L CENTIPEDE_SHIELD P2
U 1 1 571E9112
P 5450 3550
F 0 "P2" H 5100 5300 45  0000 C CNN
F 1 "CENTIPEDE_SHIELD" H 5450 3550 45  0000 C CNN
F 2 "flexi_tester_external:Arduino_mega_inverted-CENTIPEDE_SHIELD" H 5480 3700 20  0001 C CNN
F 3 "" H 5450 3550 60  0000 C CNN
	1    5450 3550
	1    0    0    -1  
$EndComp
$EndSCHEMATC
